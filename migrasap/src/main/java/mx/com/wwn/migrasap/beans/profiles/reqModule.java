/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.profiles;

import java.util.List;
import mx.com.wwn.migrasap.beans.Auth;

/**
 *
 * @author daniel
 */
public class reqModule {
    private Auth auth;
    private Data data;
    
    public reqModule(){
        super();
    }
    
    public reqModule(Auth auth, Data data){
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private profile profile;
        private List<module> modules;
        
        public Data(){
            super();
        }
        
        public Data(profile profile, List<module> modules){
            this.profile = profile;
            this.modules = modules;
        }

        public profile getProfile() {
            return profile;
        }

        public void setProfile(profile profile) {
            this.profile = profile;
        }

        public List<module> getModules() {
            return modules;
        }

        public void setModules(List<module> modules) {
            this.modules = modules;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
