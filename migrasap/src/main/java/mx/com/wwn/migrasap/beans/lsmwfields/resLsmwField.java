/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.lsmwfields;

import mx.com.wwn.migrasap.beans.Error;
import java.util.List;

/**
 *
 * @author daniel
 */
public class resLsmwField {
    private Error error;
    private Data data;
    
    public resLsmwField(){
        super();
    }

    public resLsmwField(Error error, Data data) {
        this.error = error;
        this.data = data;
    }
    
    public class Data{
        private List<lsmwField> lsmwFields;
        
        public Data(){
            super();
        }

        public Data(List<lsmwField> lsmwFields) {
            this.lsmwFields = lsmwFields;
        }

        public List<lsmwField> getLsmwFields() {
            return lsmwFields;
        }

        public void setLsmwFields(List<lsmwField> lsmwFields) {
            this.lsmwFields = lsmwFields;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
