/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.lsmws;

import java.util.List;
import mx.com.wwn.migrasap.beans.Error;
/**
 *
 * @author daniel
 */
public class resLsmw {
    private Error error;
    private Data data;
    
    public resLsmw(){
        super();
    }

    public resLsmw(Error error, Data data) {
        this.error = error;
        this.data = data;
    }
    
    public class Data{
        private List<lsmw> lsmws;
        
        public Data(){
            super();
        }

        public Data(List<lsmw> lsmws) {
            this.lsmws = lsmws;
        }

        public List<lsmw> getLsmws() {
            return lsmws;
        }

        public void setLsmws(List<lsmw> lsmws) {
            this.lsmws = lsmws;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}