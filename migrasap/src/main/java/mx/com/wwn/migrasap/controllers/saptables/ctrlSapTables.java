/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.saptables;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.beans.saptables.*;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.session;
import org.primefaces.event.RowEditEvent;
/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlSapTables")
public class ctrlSapTables {
    private int id;
    private String name;
    private List<sapTable> sapTables;
    reqSapTable req;
    resSapTable res;
    
    public ctrlSapTables() throws IOException{
        findSapTables();
    }
    
    public void findSapTables() throws IOException{
        conex conn = new conex();
        sapTables = new ArrayList<>();
        
        reqSapTable re = new reqSapTable();
        reqSapTable.Data data = re.new Data(new sapTable());
        
        req = new reqSapTable(new Auth(((login)session.getObject("user")).getUser(),"saptables","find"), data);
        res = (resSapTable) conn.sendJSON(req, resSapTable.class);
        
        if(res.getError().getCode().equals(""))for(sapTable s : res.getData().getSapTables())sapTables.add(s);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void addSapTable() throws IOException{
        conex conn = new conex();
        
        reqSapTable re = new reqSapTable();
        reqSapTable.Data data = re.new Data(new sapTable(name));
        
        req = new reqSapTable(new Auth(((login)session.getObject("user")).getUser(), "saptables", "new"), data);
        res = (resSapTable) conn.sendJSON(req, resSapTable.class);
        
        if(res.getError().getCode().equals("")){
            findSapTables();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Agregada tabla "+name));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edición cancelada", ((sapTable) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void updSapTable(RowEditEvent event) throws IOException{
        conex conn = new conex();
        
        reqSapTable re = new reqSapTable();
        reqSapTable.Data data = re.new Data(new sapTable(((sapTable) event.getObject()).getId(), ((sapTable) event.getObject()).getName()));
        
        req = new reqSapTable(new Auth(((login)session.getObject("user")).getUser(), "saptables", "edit"), data);
        res = (resSapTable) conn.sendJSON(req, resSapTable.class);
        
        if(res.getError().getCode().equals("")){
            findSapTables();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Actualizada tabla "+((sapTable) event.getObject()).getName()));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void delSapTable(int id, String name) throws IOException{
        conex conn = new conex();
        
        reqSapTable re = new reqSapTable();
        reqSapTable.Data data = re.new Data(new sapTable(id, name));
        
        req = new reqSapTable(new Auth(((login)session.getObject("user")).getUser(), "saptables", "delete"), data);
        res = (resSapTable) conn.sendJSON(req, resSapTable.class);
        
        if(res.getError().getCode().equals("")){
            findSapTables();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Eliminada tabla "+ name));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<sapTable> getSapTables() {
        return sapTables;
    }

    public void setSapTables(List<sapTable> sapTables) {
        this.sapTables = sapTables;
    }
}
