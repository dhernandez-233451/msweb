/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.login;

import mx.com.wwn.migrasap.beans.login.reqLogin;
import mx.com.wwn.migrasap.beans.login.resLogin;
import mx.com.wwn.migrasap.beans.login.login;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.primefaces.PrimeFaces;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.session;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.Error;

/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlLogin")
public class ctrlLogin {
    private String user;
    private String passwd;
    reqLogin req;
    resLogin res;
    
    String getMD5(String s) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashInBytes = md.digest(s.getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) sb.append(String.format("%02x", b));
        
        return sb.toString();
    }
    
    public String login() throws IOException, NoSuchAlgorithmException{
        boolean loggedIn;
        conex conn = new conex();
        
        reqLogin re = new reqLogin();
        reqLogin.Data data = re.new Data(new login(user, getMD5(passwd)));
        
        req = new reqLogin(new Auth(user,"login","validate"), data);
        res = (resLogin) conn.sendJSON(req, resLogin.class);
        
        if(res.getError().getCode().equals("")){
            /*HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            session.setAttribute("user", res.getData().getLogin());*/
            session.startSession("user", res.getData().getLogin());
            
            loggedIn = true;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", user));
            PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
            return "main/main.xhtml";
        }
        else{
            loggedIn = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
            PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
            return null;
        }
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }
}