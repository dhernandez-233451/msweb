/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.sapfields;

import java.util.List;
import mx.com.wwn.migrasap.beans.Error;
/**
 *
 * @author daniel
 */
public class resSapField {
    private Error error;
    private Data data;
    
    public resSapField(){
        super();
    }

    public resSapField(Error error, Data data) {
        this.error = error;
        this.data = data;
    }
    
    public class Data{
        private List<sapField> sapFields;
        
        public Data(){
            super();
        }

        public Data(List<sapField> sapFields) {
            this.sapFields = sapFields;
        }

        public List<sapField> getSapFields() {
            return sapFields;
        }

        public void setSapFields(List<sapField> sapFields) {
            this.sapFields = sapFields;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
