/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.profiles;
import java.util.List;
import mx.com.wwn.migrasap.beans.Error;
/**
 *
 * @author daniel
 */
public class resModule {
    private Error error;
    private Data data;
    
    public resModule(){
        super();
    }
    
    public resModule(Error error, Data data){
        this.error = error;
        this.data = data;
    }
    
    public class Data{
        private List<module> modules;
        
        public Data(){
            super();
        }
        
        public Data(List<module> modules){
            this.modules = modules;
        }

        public List<module> getModules() {
            return modules;
        }

        public void setModules(List<module> modules) {
            this.modules = modules;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
