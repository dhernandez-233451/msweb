/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.uploadlayout;

/**
 *
 * @author daniel
 */
public class uploadLayout {
    private int id;
    private String title;
    private String fileName;
    private String content;

    public uploadLayout() {
        super();
    }

    public uploadLayout(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public uploadLayout(int id, String title, String fileName, String content) {
        this.id = id;
        this.title = title;
        this.fileName = fileName;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFilename() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
