/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.sapconn;

import mx.com.wwn.migrasap.beans.Auth;

/**
 *
 * @author daniel
 */
public class reqSapConn {
    private Auth auth;
    private Data data;
    
    public reqSapConn(){
        super();
    }

    public reqSapConn(Auth auth, Data data) {
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private sapConn sapConn;
        
        public Data(){
            super();
        }

        public Data(sapConn sapConn) {
            this.sapConn = sapConn;
        }

        public sapConn getSapConn() {
            return sapConn;
        }

        public void setSapConn(sapConn sapConn) {
            this.sapConn = sapConn;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
