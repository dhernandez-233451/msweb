/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.localmanage;

import mx.com.wwn.migrasap.beans.Error;
/**
 *
 * @author daniel
 */
public class resLocalManage {
    private Error error;
    private Data data;

    public resLocalManage() {
        super();
    }

    public resLocalManage(Error error, Data data) {
        this.error = error;
        this.data = data;
    }
    
   public class Data{
        private localManage localManage;
        
        public Data(){
            super();
        }

        public Data(localManage localManage) {
            this.localManage = localManage;
        }

        public localManage getLocalManage() {
            return localManage;
        }

        public void setLocalManage(localManage localManage) {
            this.localManage = localManage;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}