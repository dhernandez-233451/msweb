/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.uploadlayout;

/**
 *
 * @author daniel
 */
public class layoutValidation {
    private char severity;
    private int id_field;
    private String field;
    private int row;
    private String value;
    private int id_rule;
    private String rule;
    private String comment;

    public layoutValidation() {
        super();
    }

    public layoutValidation(char severity, int id_field, String field, int row, String value, int id_rule, String rule, String comment) {
        this.severity = severity;
        this.id_field = id_field;
        this.field = field;
        this.row = row;
        this.value = value;
        this.id_rule = id_rule;
        this.rule = rule;
        this.comment = comment;
    }

    public char getSeverity() {
        return severity;
    }

    public void setSeverity(char severity) {
        this.severity = severity;
    }

    public int getId_field() {
        return id_field;
    }

    public void setId_field(int id_field) {
        this.id_field = id_field;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getId_rule() {
        return id_rule;
    }

    public void setId_rule(int id_rule) {
        this.id_rule = id_rule;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
