/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.rules;

import java.util.List;
import mx.com.wwn.migrasap.beans.Error;
/**
 *
 * @author daniel
 */
public class resRule {
    private Error error;
    private Data data;
    
    public resRule(){
        super();
    }

    public resRule(Error error, Data data) {
        this.error = error;
        this.data = data;
    }
    
    public class Data{
        private List<rule> rules;
        
        public Data(){
            super();
        }

        public Data(List<rule> rules) {
            this.rules = rules;
        }

        public List<rule> getRules() {
            return rules;
        }

        public void setRules(List<rule> rules) {
            this.rules = rules;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
