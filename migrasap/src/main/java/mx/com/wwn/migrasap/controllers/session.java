/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import mx.com.wwn.migrasap.beans.login.login;
/**
 *
 * @author daniel
 */
public class session {
    public static void startSession(String objname, Object obj){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        session.setAttribute(objname, obj);
    }
    
    public static void setObject(String objname, Object obj){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.setAttribute(objname, obj);
    }
    
    public static Object getObject(String objname){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        return session.getAttribute(objname);
    }
    
    public static void removeObject(String objname){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.removeAttribute(objname);
    }
    
    public static void endSession(){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.invalidate();
    }
}
