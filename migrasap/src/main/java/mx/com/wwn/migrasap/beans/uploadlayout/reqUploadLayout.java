/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.uploadlayout;

import mx.com.wwn.migrasap.beans.Auth;

/**
 *
 * @author daniel
 */
public class reqUploadLayout {
    private Auth auth;
    private Data data;

    public reqUploadLayout() {
        super();
    }

    public reqUploadLayout(Auth auth, Data data) {
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private uploadLayout uploadLayout;

        public Data() {
            super();
        }

        public Data(uploadLayout uploadLayout) {
            this.uploadLayout = uploadLayout;
        }

        public uploadLayout getUploadLayout() {
            return uploadLayout;
        }

        public void setUploadLayout(uploadLayout uploadLayout) {
            this.uploadLayout = uploadLayout;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
