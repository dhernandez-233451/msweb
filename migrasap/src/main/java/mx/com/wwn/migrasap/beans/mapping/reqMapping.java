/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.mapping;

import java.util.List;
import mx.com.wwn.migrasap.beans.Auth;

/**
 *
 * @author daniel
 */
public class reqMapping {
    private Auth auth;
    private Data data;
    
    public reqMapping(){
        super();
    }

    public reqMapping(Auth auth, Data data) {
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private int id_lsmw;
        private List<mapping> mapping;
        
        public Data(){
            super();
        }

        public Data(int id_lsmw) {
            this.id_lsmw = id_lsmw;
        }

        public Data(List<mapping> mapping) {
            this.mapping = mapping;
        }

        public int getId_lsmw() {
            return id_lsmw;
        }

        public void setId_lsmw(int id_lsmw) {
            this.id_lsmw = id_lsmw;
        }

        public List<mapping> getMapping() {
            return mapping;
        }

        public void setMapping(List<mapping> mapping) {
            this.mapping = mapping;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
