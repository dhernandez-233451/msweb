/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.localmanage;

import mx.com.wwn.migrasap.beans.Error;
/**
 *
 * @author daniel
 */
public class resMassiveImport {
    private Error error;

    public resMassiveImport() {
        super();
    }

    public resMassiveImport(Error error) {
        this.error = error;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
