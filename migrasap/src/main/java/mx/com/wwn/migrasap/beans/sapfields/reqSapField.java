/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.sapfields;

import mx.com.wwn.migrasap.beans.Auth;

/**
 *
 * @author daniel
 */
public class reqSapField {
    private Auth auth;
    private Data data;
    
    public reqSapField(){
        super();
    }

    public reqSapField(Auth auth, Data data) {
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private sapField sapField;
        
        public Data(){
            super();
        }

        public Data(sapField sapField) {
            this.sapField = sapField;
        }

        public sapField getSapField() {
            return sapField;
        }

        public void setSapField(sapField sapField) {
            this.sapField = sapField;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
