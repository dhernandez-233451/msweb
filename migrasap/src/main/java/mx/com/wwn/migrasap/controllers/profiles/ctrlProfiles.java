/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.profiles;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.profiles.*;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.beans.login.resLogin;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.session;
import org.primefaces.PrimeFaces;
import org.primefaces.event.RowEditEvent;
/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlProfiles")
public class ctrlProfiles {
    private int id;
    private String name;
    private List<profile> profiles;
    
    reqProfile req;
    resProfile res;
    
    public ctrlProfiles() throws IOException{
        session.removeObject("profile_edit");
        findProfiles();
    }
        
    public void findProfiles() throws IOException{
        conex conn = new conex();
        profiles = new ArrayList<>();
        
        reqProfile re = new reqProfile();
        reqProfile.Data data = re.new Data(new profile());
        
        req = new reqProfile(new Auth(((login)session.getObject("user")).getUser(),"profiles","find"), data);
        res = (resProfile) conn.sendJSON(req, resProfile.class);
        
        if(res.getError().getCode().equals(""))for(profile p : res.getData().getProfiles())profiles.add(p);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void addProfile() throws IOException{
        conex conn = new conex();
        
        reqProfile re = new reqProfile();
        reqProfile.Data data = re.new Data(new profile(name));
        
        req = new reqProfile(new Auth(((login)session.getObject("user")).getUser(),"profiles","new"), data);
        res = (resProfile) conn.sendJSON(req, resProfile.class);
        
        if(res.getError().getCode().equals("")){
            findProfiles();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Agregado perfil "+name));
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
        }
    }
    
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edición cancelada", ((profile) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void updProfile(RowEditEvent event) throws IOException{
        conex conn = new conex();
        
        reqProfile re = new reqProfile();
        reqProfile.Data data = re.new Data(new profile(((profile) event.getObject()).getId(), ((profile) event.getObject()).getName()));
        
        req = new reqProfile(new Auth(((login)session.getObject("user")).getUser(),"profiles","edit"), data);
        res = (resProfile) conn.sendJSON(req, resProfile.class);
        
        if(res.getError().getCode().equals("")){
            findProfiles();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Actualizado perfil "+((profile) event.getObject()).getName()));
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
        }
    }
    
    public void delProfile(int id, String name) throws IOException{
        conex conn = new conex();
        
        reqProfile re = new reqProfile();
        reqProfile.Data data = re.new Data(new profile(id, name));
        
        req = new reqProfile(new Auth(((login)session.getObject("user")).getUser(),"profiles","delete"), data);
        res = (resProfile) conn.sendJSON(req, resProfile.class);
        
        if(res.getError().getCode().equals("")){
            findProfiles();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Eliminado perfil "+name));
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
        }
    }
    
    public String goPermits(int id, String name){
        profile p = new profile(id, name);
        session.setObject("profile_edit", p);
        return "/profiles/modules";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<profile> profiles) {
        this.profiles = profiles;
    }
}
