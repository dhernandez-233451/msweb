/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.sapconn;

import com.sap.conn.jco.JCoException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.beans.sapconn.*;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.rfc.RFC;
import mx.com.wwn.migrasap.controllers.session;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlSapConn")
public class ctrlSapConn {
    private static int id;
    private String name;
    private String host;
    private String sysnr;
    private String client;
    private String user;
    private String passwd;
    private String lang;
    private String saprouter;
    private String function;
    private List<sapConn> sapConns;
    reqSapConn req;
    resSapConn res;
    
    public ctrlSapConn() throws IOException{
        findSapConns();
    }
    
    public void findSapConns() throws IOException{
        conex conn = new conex();
        sapConns = new ArrayList<>();
        
        reqSapConn re = new reqSapConn();
        reqSapConn.Data data = re.new Data(new sapConn());
        
        req = new reqSapConn(new Auth(((login) session.getObject("user")).getUser(),"sapconn","find"), data);
        res = (resSapConn) conn.sendJSON(req, resSapConn.class);
        
        if(res.getError().getCode().equals(""))for(sapConn s : res.getData().getSapConns()) sapConns.add(s);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    String getMD5(String s) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashInBytes = md.digest(s.getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) sb.append(String.format("%02x", b));
        
        return sb.toString();
    }
    
    public void addSapConn() throws IOException, NoSuchAlgorithmException{
        conex conn = new conex();
        sapConns = new ArrayList<>();
        
        reqSapConn re = new reqSapConn();
        //reqSapConn.Data data = re.new Data(new sapConn(name, host,sysnr, client, user, getMD5(passwd), lang, saprouter, function));
        reqSapConn.Data data = re.new Data(new sapConn(name, host,sysnr, client, user, null, lang, saprouter, function));
        
        req = new reqSapConn(new Auth(((login) session.getObject("user")).getUser(),"sapconn","new"), data);
        res = (resSapConn) conn.sendJSON(req, resSapConn.class);
        
        if(res.getError().getCode().equals("")){
            findSapConns();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Agregada conexión "+name));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edición cancelada", ((sapConn) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void updSapConn(RowEditEvent event) throws IOException, NoSuchAlgorithmException{
        conex conn = new conex();
        sapConns = new ArrayList<>();
        
        reqSapConn re = new reqSapConn();
        //reqSapConn.Data data = re.new Data(new sapConn(((sapConn) event.getObject()).getId(),((sapConn) event.getObject()).getName(), ((sapConn) event.getObject()).getHost(),((sapConn) event.getObject()).getSysnr(), ((sapConn) event.getObject()).getClient(), ((sapConn) event.getObject()).getUser(), getMD5(((sapConn) event.getObject()).getPasswd()), ((sapConn) event.getObject()).getLang(), ((sapConn) event.getObject()).getSaprouter(), ((sapConn) event.getObject()).getFunction()));
        reqSapConn.Data data = re.new Data(new sapConn(((sapConn) event.getObject()).getId(),((sapConn) event.getObject()).getName(), ((sapConn) event.getObject()).getHost(),((sapConn) event.getObject()).getSysnr(), ((sapConn) event.getObject()).getClient(), ((sapConn) event.getObject()).getUser(), null, ((sapConn) event.getObject()).getLang(), ((sapConn) event.getObject()).getSaprouter(), ((sapConn) event.getObject()).getFunction()));
        
        req = new reqSapConn(new Auth(((login) session.getObject("user")).getUser(),"sapconn","edit"), data);
        res = (resSapConn) conn.sendJSON(req, resSapConn.class);
        
        if(res.getError().getCode().equals("")){
            findSapConns();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Actualizada conexión "+((sapConn) event.getObject()).getName()));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void delSapConn(int id, String name) throws IOException{
        conex conn = new conex();
        sapConns = new ArrayList<>();
        
        reqSapConn re = new reqSapConn();
        reqSapConn.Data data = re.new Data(new sapConn(id, name, null, null, null, null, null, null, null, null));
        
        req = new reqSapConn(new Auth(((login) session.getObject("user")).getUser(),"sapconn","delete"), data);
        res = (resSapConn) conn.sendJSON(req, resSapConn.class);
        
        if(res.getError().getCode().equals("")){
            findSapConns();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Eliminada conexión "+name));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void testConnection() throws JCoException{
        RFC rfc = new RFC();
        List<String> fields;
        sapConn sc = null;
        
        for(sapConn c : sapConns)if(c.getId() == id){
            sc = c;
            break;
        }
        
        rfc.setConnectionParameters(sc.getHost(), sc.getSysnr(), sc.getClient(), sc.getUser(), passwd, sc.getLang(), sc.getSaprouter(), sc.getFunction());
        rfc.extractData("SPFLI", new ArrayList<String>());
        fields = rfc.fields;
        
        id = 0;
        passwd = "";
        if(fields.size() > 0) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Pureba de conexión exitosa  "+sc.getName()));
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "No se consiguió la conexión a "+sc.getName()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getSysnr() {
        return sysnr;
    }

    public void setSysnr(String sysnr) {
        this.sysnr = sysnr;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getSaprouter() {
        return saprouter;
    }

    public void setSaprouter(String saprouter) {
        this.saprouter = saprouter;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public List<sapConn> getSapConns() {
        return sapConns;
    }

    public void setSapConns(List<sapConn> sapConns) {
        this.sapConns = sapConns;
    }
}