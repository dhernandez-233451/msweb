/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.users;

/**
 *
 * @author daniel
 */
public class user {
    private int id;
    private int id_profile;
    private String profile;
    private String name;
    private String user;
    private String passwd;
    private String session;
    private String access_attempt;
    
    public user(){
        super();
    }
    
    public user(int id_profile, String name, String user, String passwd){
        this.id_profile = id_profile;
        this.name = name;
        this.user = user;
        this.passwd = passwd;
    }
    
    public user(int id, int id_profile, String name, String user, String passwd){
        this.id = id;
        this.id_profile = id_profile;
        this.name = name;
        this.user = user;
        this.passwd = passwd;
    }
    
    public user(int id, int id_profile, String profile, String name, String user, String passwd, String session, String access_attempt){
        this.id = id;
        this.id_profile = id_profile;
        this.profile = profile;
        this.name = name;
        this.user = user;
        this.passwd = passwd;
        this.session = session;
        this.access_attempt = access_attempt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_profile() {
        return id_profile;
    }

    public void setId_profile(int id_profile) {
        this.id_profile = id_profile;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getAccess_attempt() {
        return access_attempt;
    }

    public void setAccess_attempt(String access_attempt) {
        this.access_attempt = access_attempt;
    }
}
