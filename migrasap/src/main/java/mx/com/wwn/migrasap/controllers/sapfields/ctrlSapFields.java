/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.sapfields;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.beans.sapfields.*;
import mx.com.wwn.migrasap.beans.saptables.*;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.session;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlSapFields")
public class ctrlSapFields {
    private int id;
    private static int id_sap_table;
    private String sap_table;
    private String name;
    private List<sapTable> sapTables;
    private List<sapField> sapFields;    
    reqSapField req;
    resSapField res;
    
    public ctrlSapFields() throws IOException{
        findSapTables();
        findSapFields();
    }
    
    public void findSapTables() throws IOException{        
        conex conn = new conex();
        sapTables = new ArrayList<>();
        
        reqSapTable re = new reqSapTable();
        reqSapTable.Data data = re.new Data(new sapTable());
        
        reqSapTable req0 = new reqSapTable(new Auth(((login)session.getObject("user")).getUser(),"saptables","find"), data);
        resSapTable res0 = (resSapTable) conn.sendJSON(req0, resSapTable.class);
        
        if(res0.getError().getCode().equals(""))for(sapTable s : res0.getData().getSapTables())sapTables.add(s);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res0.getError().getCode(), res0.getError().getMessage()));
    }
    
    public void findSapFields() throws IOException{
        conex conn = new conex();
        sapFields = new ArrayList<>();
        
        reqSapField re = new reqSapField();
        reqSapField.Data data = re.new Data(new sapField(id_sap_table));
        
        req = new reqSapField(new Auth(((login)session.getObject("user")).getUser(), "sapfields", "find"), data);
        res = (resSapField) conn.sendJSON(req, resSapField.class);
        
        if(res.getError().getCode().equals(""))for(sapField s : res.getData().getSapFields())sapFields.add(s);
        //else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void addSapField() throws IOException{
        conex conn = new conex();
        
        reqSapField re = new reqSapField();
        reqSapField.Data data = re.new Data(new sapField(id_sap_table, name));
        
        req = new reqSapField(new Auth(((login)session.getObject("user")).getUser(), "sapfields", "new"), data);
        res = (resSapField) conn.sendJSON(req, resSapField.class);
        
        if(res.getError().getCode().equals("")){
            findSapFields();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Agregado campo "+name));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edición cancelada", ((sapField) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void updSapField(RowEditEvent event) throws IOException{
        conex conn = new conex();
        
        reqSapField re = new reqSapField();
        reqSapField.Data data = re.new Data(new sapField(((sapField) event.getObject()).getId(), ((sapField) event.getObject()).getName()));
        
        req = new reqSapField(new Auth(((login)session.getObject("user")).getUser(), "sapfields", "edit"), data);
        res = (resSapField) conn.sendJSON(req, resSapField.class);
        
        if(res.getError().getCode().equals("")){
            findSapFields();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Actualizado campo "+((sapField) event.getObject()).getName()));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void delSapField(int id, String name) throws IOException{
        conex conn = new conex();
        
        reqSapField re = new reqSapField();
        reqSapField.Data data = re.new Data(new sapField(id));
        
        req = new reqSapField(new Auth(((login)session.getObject("user")).getUser(), "sapfields", "delete"), data);
        res = (resSapField) conn.sendJSON(req, resSapField.class);
        
        if(res.getError().getCode().equals("")){
            findSapFields();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Eliminado campo "+name));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_sap_table() {
        return id_sap_table;
    }

    public void setId_sap_table(int id_sap_table) {
        this.id_sap_table = id_sap_table;
    }

    public String getSap_table() {
        return sap_table;
    }

    public void setSap_table(String sap_table) {
        this.sap_table = sap_table;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<sapTable> getSapTables() {
        return sapTables;
    }

    public void setSapTables(List<sapTable> sapTables) {
        this.sapTables = sapTables;
    }

    public List<sapField> getSapFields() {
        return sapFields;
    }

    public void setSapFields(List<sapField> sapFields) {
        this.sapFields = sapFields;
    }
}
