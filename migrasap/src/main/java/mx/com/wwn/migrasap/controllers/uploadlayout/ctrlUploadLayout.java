/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.uploadlayout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.beans.lsmws.*;
import mx.com.wwn.migrasap.beans.uploadlayout.*;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.files;
import mx.com.wwn.migrasap.controllers.session;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlUploadLayout")
public class ctrlUploadLayout {
    private static int id;
    private static String title;
    private List<lsmw> lsmws;
    private uploadLayout uploadLayout;
    private List<layoutValidation> layoutValidations;
    
    static String fileName;
    reqUploadLayout req;
    resUploadLayout res;
    
    public ctrlUploadLayout() throws IOException{
        findLsmws();
        //btnAtm(false);
    }
    
    void btnAtm(boolean display){
        UIViewRoot view = FacesContext.getCurrentInstance().getViewRoot();
        UIComponent component = view.findComponent("frm_validations:btnATM");
        if(display){
            //component.getAttributes().put("disabled", "true");
            component.getAttributes().put("style", "display:block");
        }
        else{
            //component.getAttributes().put("disabled", "false");
            component.getAttributes().put("style", "display:none");
        }
    }
    
    boolean validateAutoCorrections(){
        boolean atm = true;
        if(layoutValidations != null){
            for(layoutValidation lv : layoutValidations)if(lv.getSeverity() == 'H'){
                    atm = false;
                    break;
                }
        }
        else atm = false;
        return atm;
    }
    
    public void selectedLSMW(){
        for(lsmw l: lsmws)if(l.getId() == id){
            title = l.getTitle();
            break;
        }
    }
    
    public void findLsmws() throws IOException{
        conex conn = new conex();
        reqLsmw req0;
        resLsmw res0;
        lsmws = new ArrayList<>();
        
        reqLsmw re = new reqLsmw();
        reqLsmw.Data data = re.new Data(new lsmw());
        
        req0 = new reqLsmw(new Auth(((login)session.getObject("user")).getUser(),"lsmws","find"), data);
        res0 = (resLsmw) conn.sendJSON(req0, resLsmw.class);
        
        if(res0.getError().getCode().equals(""))for(lsmw l : res0.getData().getLsmws())lsmws.add(l);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res0.getError().getCode(), res0.getError().getMessage()));
    }
    
    public void uploadLayout(FileUploadEvent event) throws IOException{
        if(id != 0){
            conex conn = new conex();
            files f = new files();
            layoutValidations = new ArrayList<>();
            fileName = event.getFile().getFileName();

            reqUploadLayout re = new reqUploadLayout();
            reqUploadLayout.Data data = re.new Data(new uploadLayout(id, title, fileName, f.convertToBase64(event.getFile())));

            req = new reqUploadLayout(new Auth(((login) session.getObject("user")).getUser(), "uploadlayouts", "upload"),data);
            res = (resUploadLayout) conn.sendJSON(req, resUploadLayout.class);

            if(res.getError().getCode().equals("")) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Carga correcta", title));
            else {
                uploadLayout = res.getData().getUploadLayout();
                layoutValidations = res.getData().getLayoutValidations();
                btnAtm(validateAutoCorrections());
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
            }
        }
        else  FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error: ", "Debe elegír un LSMW de la lista"));
    }
    
    public void applyAutoCorrections() throws IOException{
        conex conn = new conex();
        
        reqUploadLayout re = new reqUploadLayout();
        reqUploadLayout.Data data = re.new Data(new uploadLayout(id, title, fileName, null));
        
        req = new reqUploadLayout(new Auth(((login) session.getObject("user")).getUser(), "uploadlayouts", "autocorrections"),data);
        res = (resUploadLayout) conn.sendJSON(req, resUploadLayout.class);
        
        if(res.getError().getCode().equals("")){
            layoutValidations = null;
            btnAtm(validateAutoCorrections());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Carga correcta", title));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<lsmw> getLsmws() {
        return lsmws;
    }

    public void setLsmws(List<lsmw> lsmws) {
        this.lsmws = lsmws;
    }

    public uploadLayout getUploadLayout() {
        return uploadLayout;
    }

    public void setUploadLayout(uploadLayout uploadLayout) {
        this.uploadLayout = uploadLayout;
    }

    public List<layoutValidation> getLayoutValidations() {
        return layoutValidations;
    }

    public void setLayoutValidations(List<layoutValidation> layoutValidations) {
        this.layoutValidations = layoutValidations;
    }
}
