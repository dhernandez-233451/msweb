/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.rules;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author daniel
 */
public class rule {
    private int id_lsmw_field;
    private int id_rule;
    private String rule;
    private boolean active;
    
    public rule(){
        super();
    }

    public rule(int id_lsmw_field) {
        this.id_lsmw_field = id_lsmw_field;
    }

    public rule(int id_lsmw_field, int id_rule, String rule, boolean active) {
        this.id_lsmw_field = id_lsmw_field;
        this.id_rule = id_rule;
        this.rule = rule;
        this.active = active;
    }

    public int getId_lsmw_field() {
        return id_lsmw_field;
    }

    public void setId_lsmw_field(int id_lsmw_field) {
        this.id_lsmw_field = id_lsmw_field;
    }

    public int getId_rule() {
        return id_rule;
    }

    public void setId_rule(int id_rule) {
        this.id_rule = id_rule;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}

