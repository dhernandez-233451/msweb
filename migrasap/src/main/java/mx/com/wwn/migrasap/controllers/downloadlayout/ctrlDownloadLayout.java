/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.downloadlayout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.downloadlayout.*;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.beans.lsmws.*;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.files;
import mx.com.wwn.migrasap.controllers.session;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlDownloadLayout")
public class ctrlDownloadLayout {
    private int id;
    private String title;
    private List<lsmw> lsmws;
    //private List<downloadLayout> downloadLayouts;
    
    reqDownloadLayout req;
    resDownloadLayout res;
    
    public ctrlDownloadLayout() throws IOException{
        findLsmws();
    }
    
    public void findLsmws() throws IOException{
        conex conn = new conex();
        reqLsmw req0;
        resLsmw res0;
        lsmws = new ArrayList<>();
        
        reqLsmw re = new reqLsmw();
        reqLsmw.Data data = re.new Data(new lsmw());
        
        req0 = new reqLsmw(new Auth(((login)session.getObject("user")).getUser(),"lsmws","find"), data);
        res0 = (resLsmw) conn.sendJSON(req0, resLsmw.class);
        
        if(res0.getError().getCode().equals(""))for(lsmw l : res0.getData().getLsmws())lsmws.add(l);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res0.getError().getCode(), res0.getError().getMessage()));
    }
    
    public StreamedContent downloadLayout(int id, String title) throws IOException{
        conex conn = new conex();
        files f = new files();
        
        reqDownloadLayout re = new reqDownloadLayout();
        reqDownloadLayout.Data data = re.new Data(new downloadLayout(id));
        
        req = new reqDownloadLayout(new Auth(((login) session.getObject("user")).getUser(), "downloadlayouts", "download"), data);
        res = (resDownloadLayout) conn.sendJSON(req, resDownloadLayout.class);
        
        if(res.getError().getCode().equals("")){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Excelente", title));
            return f.convertToFile(res.getData().getDownloadLayout().getTitle(), res.getData().getDownloadLayout().getContent());
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
            return null;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<lsmw> getLsmws() {
        return lsmws;
    }

    public void setLsmws(List<lsmw> lsmws) {
        this.lsmws = lsmws;
    }
}
