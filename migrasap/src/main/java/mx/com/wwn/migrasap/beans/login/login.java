/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.login;

/**
 *
 * @author daniel
 */
public class login {
    private int id;
    private String user;
    private String name;
    private String passwd;
    private String profile;
    private int[] permits;
    
    public login(){
        super();
    }
    
    public login(String user, String passwd){
        this.user = user;
        this.passwd = passwd;
    }
    
    public login(int id, String user, String name, String profile, int[] permits){
        this.id = id;
        this.user = user;
        this.name = name;
        this.profile = profile;
        this.permits = permits;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public int[] getPermits() {
        return permits;
    }

    public void setPermits(int[] permits) {
        this.permits = permits;
    }
}