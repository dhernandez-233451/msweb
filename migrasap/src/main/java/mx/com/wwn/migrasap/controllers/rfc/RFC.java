/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.rfc;

import com.sap.conn.jco.AbapException;
import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoTable;
import com.sap.conn.jco.ext.DestinationDataProvider;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;



/**
 *
 * @author daniel
 */
public class RFC {
    public List<String> fields = new ArrayList<String>();
    public List<String[]> data = new ArrayList<String[]>();
            
    JCoDestination destination;
    JCoFunction function;
    String functionName = "";
    String delimiter = "<";
    int rowsBuffer = 200;
    
    public void setConnectionParameters(String host, String sysnr, String client, String user, String passwd, String lang, String saprouter, String functionName){
        sapjco.connectProperties = new Properties();
        sapjco.connectProperties.setProperty(DestinationDataProvider.JCO_ASHOST, host);
        sapjco.connectProperties.setProperty(DestinationDataProvider.JCO_SYSNR, sysnr);
        sapjco.connectProperties.setProperty(DestinationDataProvider.JCO_CLIENT, client);
        sapjco.connectProperties.setProperty(DestinationDataProvider.JCO_USER, user);
        sapjco.connectProperties.setProperty(DestinationDataProvider.JCO_PASSWD, passwd);
        sapjco.connectProperties.setProperty(DestinationDataProvider.JCO_LANG, lang);
        if(!saprouter.equals(""))sapjco.connectProperties.setProperty(DestinationDataProvider.JCO_SAPROUTER, "/H/"+saprouter+"/H/");
        this.functionName = functionName;
        
        sapjco.createDataFile(sapjco.ABAP_AS, "jcoDestination", sapjco.connectProperties);
        sapjco.connectProperties.setProperty(DestinationDataProvider.JCO_POOL_CAPACITY, "3");
        sapjco.connectProperties.setProperty(DestinationDataProvider.JCO_PEAK_LIMIT, "10");
        sapjco.createDataFile(sapjco.ABAP_AS_POOLED, "jcoDestination", sapjco.connectProperties);
    }
        
    void takeFields(String fn){
        fields.add(fn.trim().equals("")?" ":fn.trim());
    }
    
    void takeData(String wa){
        String[] row = wa.split(delimiter);
        for(int i = 0; i < row.length; i++)row[i] = (row[i].trim().equals("")?" ":row[i].trim());
        data.add(row);
    }
    
    public void extractData(String table, List<String> lstFields) throws JCoException{
        int totalRows = 0;
        int counter = 0;
        int segment = 0;
        JCoTable tblFlieds_in;
        JCoTable tblFields_out;
        JCoTable tblData_out;
        
        
        destination = JCoDestinationManager.getDestination(sapjco.ABAP_AS_POOLED);
        function = destination.getRepository().getFunction(this.functionName);
        if (function == null) {
            throw new RuntimeException(this.functionName +" not found in SAP.");
        }
        try {
            function.execute(destination);
        } catch (AbapException e) {
            System.out.println(e.toString());
            return;
        }
        
        function.getImportParameterList().setValue("QUERY_TABLE", table);
        function.getImportParameterList().setValue("DELIMITER", delimiter);
        function.getImportParameterList().setValue("COUNT_ROWS", "X");
        function.execute(destination);
        
        function.getImportParameterList().setValue("COUNT_ROWS", "");
        
        tblFlieds_in = function.getTableParameterList().getTable("FIELDS");
        for(String f : lstFields){
            tblFlieds_in.appendRow();
            tblFlieds_in.setValue("FIELDNAME", f);
        }
        
        totalRows = Integer.parseInt(function.getExportParameterList().getValue("TOTAL_ROWS").toString().trim());
        
        while (counter < totalRows){
            segment = (totalRows - counter > rowsBuffer)?  rowsBuffer : totalRows - counter;
            function.getImportParameterList().setValue("ROWSKIPS", counter);
            function.getImportParameterList().setValue("ROWCOUNT", segment);
            function.execute(destination);
            
            tblFields_out = function.getTableParameterList().getTable("FIELDS");
            tblData_out = function.getTableParameterList().getTable("DATA");
            
            if(counter == 0)for (int i = 0; i < tblFields_out.getNumRows(); i++){
                    takeFields(tblFields_out.getValue("FIELDNAME").toString());
                    tblFields_out.nextRow();
                }
            for (int i = 0; i < tblData_out.getNumRows(); i++){
                takeData(tblData_out.getValue("WA").toString());
                tblData_out.nextRow();
            }
            
            counter += rowsBuffer;
        }
    }
}
