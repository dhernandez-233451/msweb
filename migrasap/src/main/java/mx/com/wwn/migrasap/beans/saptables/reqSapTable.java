/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.saptables;

import mx.com.wwn.migrasap.beans.Auth;

/**
 *
 * @author daniel
 */
public class reqSapTable {
    private Auth auth;
    private Data data;
    
    public reqSapTable(){
        super();
    }

    public reqSapTable(Auth auth, Data data) {
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private sapTable sapTable;
        
        public Data(){
            super();
        }

        public Data(sapTable sapTable) {
            this.sapTable = sapTable;
        }

        public sapTable getSapTable() {
            return sapTable;
        }

        public void setSapTable(sapTable sapTable) {
            this.sapTable = sapTable;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
