/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.login;

import mx.com.wwn.migrasap.beans.Auth;
/**
 *
 * @author daniel
 */
public class reqLogin {
    private Auth auth;
    private Data data;
    
    public reqLogin(){
        super();
    }
    
    public reqLogin(Auth auth, Data data){
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private login login;
        
        public Data(){
            super();
        }
        
        public Data(login login){
            this.login = login;
        }
        
        public login getLogin(){
            return this.login;
        }
        
        public void setLogin(login login){
            this.login = login;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}