/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.rules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.beans.lsmwfields.*;
import mx.com.wwn.migrasap.beans.lsmws.*;
import mx.com.wwn.migrasap.beans.rules.*;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.session;

/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlRules")
public class ctrlRules {
    private static int id_lsmw;
    private String lsmw;
    private static int id_lsmw_field;
    private String lsmw_field;
    private List<lsmw> lsmws;
    private List<lsmwField> lsmwFields;
    private static List<rule> rules;
    
    reqRule req;
    resRule res;
    
    public ctrlRules() throws IOException{
        findLsmws();
        findLsmwFields();
    }
    
    public void findLsmws() throws IOException{
        conex conn = new conex();
        lsmws = new ArrayList<>();
        
        reqLsmw re = new reqLsmw();
        reqLsmw.Data data = re.new Data(new lsmw());
        
        reqLsmw req0 = new reqLsmw(new Auth(((login)session.getObject("user")).getUser(),"lsmws","find"), data);
        resLsmw res0 = (resLsmw) conn.sendJSON(req0, resLsmw.class);
        
        if(res0.getError().getCode().equals(""))for(lsmw l : res0.getData().getLsmws())lsmws.add(l);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res0.getError().getCode(), res0.getError().getMessage()));
    }
    
    public void findLsmwFields() throws IOException{
        conex conn = new conex();
        lsmwFields = new ArrayList<>();
        
        reqLsmwField re = new reqLsmwField();
        reqLsmwField.Data data = re.new Data(new lsmwField(id_lsmw));
        
        reqLsmwField req0 = new reqLsmwField(new Auth(((login)session.getObject("user")).getUser(), "lsmwfields", "find"), data);
        resLsmwField res0 = (resLsmwField) conn.sendJSON(req0, resLsmwField.class);
        
        if(res0.getError().getCode().equals(""))for(lsmwField lf : res0.getData().getLsmwFields())lsmwFields.add(lf);
        //else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res0.getError().getCode(), res0.getError().getMessage()));
    }
    
    public void findRules() throws IOException{
        conex conn = new conex();
        rules = new ArrayList<>();
        
        List<rule> lr = new ArrayList<>();
        lr.add(new rule(id_lsmw_field));
        
        reqRule re = new reqRule();
        reqRule.Data data = re.new Data(lr);
        
        req = new reqRule(new Auth(((login)session.getObject("user")).getUser(), "rules", "find"), data);
        res = (resRule) conn.sendJSON(req, resRule.class);
        
        if(res.getError().getCode().equals("")) for(rule r : res.getData().getRules())rules.add(r);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void updRules() throws IOException{
        conex conn = new conex();
        
        reqRule re = new reqRule();
        reqRule.Data data = re.new Data(rules);
        
        req = new reqRule(new Auth(((login)session.getObject("user")).getUser(), "rules", "edit"), data);
        res = (resRule) conn.sendJSON(req, resRule.class);
        
        if(res.getError().getCode().equals("")){
            findRules();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Actualizadas reglas"));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }

    public int getId_lsmw() {
        return id_lsmw;
    }

    public void setId_lsmw(int id_lsmw) {
        this.id_lsmw = id_lsmw;
    }

    public String getLsmw() {
        return lsmw;
    }

    public void setLsmw(String lsmw) {
        this.lsmw = lsmw;
    }

    public int getId_lsmw_field() {
        return id_lsmw_field;
    }

    public void setId_lsmw_field(int id_lsmw_field) {
        this.id_lsmw_field = id_lsmw_field;
    }

    public String getLsmw_field() {
        return lsmw_field;
    }

    public void setLsmw_field(String lsmw_field) {
        this.lsmw_field = lsmw_field;
    }

    public List<lsmw> getLsmws() {
        return lsmws;
    }

    public void setLsmws(List<lsmw> lsmws) {
        this.lsmws = lsmws;
    }

    public List<lsmwField> getLsmwFields() {
        return lsmwFields;
    }

    public void setLsmwFields(List<lsmwField> lsmwFields) {
        this.lsmwFields = lsmwFields;
    }

    public List<rule> getRules() {
        return rules;
    }

    public void setRules(List<rule> rules) {
        this.rules = rules;
    }

    public reqRule getReq() {
        return req;
    }

    public void setReq(reqRule req) {
        this.req = req;
    }

    public resRule getRes() {
        return res;
    }

    public void setRes(resRule res) {
        this.res = res;
    }
}

/*
public class ctrlRules {
    private List<Columna> columnas = new ArrayList<>();
    private List<HashMap<String,String>> reglas = new ArrayList<>();
    
    
    public ctrlRules(){
        for (int i = 0; i < 10; i++) {
            HashMap<String,String> r = new HashMap<String,String>();
            for (int j = 0; j < 5; j++) {
                if (i == 0) {
                    columnas.add(new Columna("Nombre"+j,"nombre"+j));
                }
                r.put("nombre"+j, "vaule l_"+i+" c_"+j);
            }
            reglas.add(r);
        }
    }
    
    public class Columna{
        private String nombre;
        private String propiedad;
        public Columna(String n, String p){
            nombre = n;
            propiedad =p;
        }
        public void setNombre(String n){nombre = n;}
        public String getNombre(){return nombre;}
        public void setPropiedad(String p){propiedad = p;}
        public String getPropiedad(){return propiedad;}
    }

    public List<Columna> getColumnas() {
        return columnas;
    }

    public void setColumnas(List<Columna> columnas) {
        this.columnas = columnas;
    }

    public List<HashMap<String, String>> getReglas() {
        return reglas;
    }

    public void setReglas(List<HashMap<String, String>> reglas) {
        this.reglas = reglas;
    }
}

<p:dataTable value="#{ctrlRules.reglas}" var="r" scrollable="true" scrollWidth="1000"  frozenColumns="1" >
                <p:columns value="#{ctrlRules.columnas}" var="key" headerText="#{key.nombre}" >
                    <h:outputText value="#{r[key.propiedad]}" />
		</p:columns>
	    </p:dataTable>
*/