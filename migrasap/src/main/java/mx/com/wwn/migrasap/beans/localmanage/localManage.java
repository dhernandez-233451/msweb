/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.localmanage;

/**
 *
 * @author daniel
 */
public class localManage {
    private String dbname;
    private String content;
    
    public localManage(){
        super();
    }

    public localManage(String dbname) {
        this.dbname = dbname;
    }

    public localManage(String dbname, String content) {
        this.dbname = dbname;
        this.content = content;
    }

    public String getDbname() {
        return dbname;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
