/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.uploadlayout;

import java.util.List;
import mx.com.wwn.migrasap.beans.Error;
/**
 *
 * @author daniel
 */
public class resUploadLayout {
    private Error error;
    private Data data;

    public resUploadLayout() {
        super();
    }

    public resUploadLayout(Error error, Data data) {
        this.error = error;
        this.data = data;
    }
    
    public class Data{
        private uploadLayout uploadLayout;
        private List<layoutValidation> layoutValidations;

        public Data() {
            super();
        }

        public Data(uploadLayout uploadLayout, List<layoutValidation> layoutValidations) {
            this.uploadLayout = uploadLayout;
            this.layoutValidations = layoutValidations;
        }

        public uploadLayout getUploadLayout() {
            return uploadLayout;
        }

        public void setUploadLayout(uploadLayout uploadLayout) {
            this.uploadLayout = uploadLayout;
        }

        public List<layoutValidation> getLayoutValidations() {
            return layoutValidations;
        }

        public void setLayoutValidations(List<layoutValidation> layoutValidations) {
            this.layoutValidations = layoutValidations;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
