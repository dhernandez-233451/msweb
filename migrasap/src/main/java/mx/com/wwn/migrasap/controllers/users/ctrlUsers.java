/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.users;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.session;
import mx.com.wwn.migrasap.beans.users.*;
import mx.com.wwn.migrasap.beans.profiles.profile;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlUsers")
public class ctrlUsers {
    private int id_profile;
    private String name;
    private String user;
    private String passwd;
    private List<profile> profiles;
    private List<user> users;
    reqUser req;
    resUser res;
    
    public ctrlUsers() throws IOException{
        findUsers();
    }
    
    public void findUsers() throws IOException{
        conex conn = new conex();
        profiles = new ArrayList<>();
        users = new ArrayList<>();
        
        reqUser re = new reqUser();
        reqUser.Data data = re.new Data(new user());
        
        req = new reqUser(new Auth(((login)session.getObject("user")).getUser(),"users","find"), data);
        res = (resUser) conn.sendJSON(req, resUser.class);
        
        if(res.getError().getCode().equals("")){
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),""));
            for(profile p : res.getData().getProfiles())profiles.add(p);
            for(user u : res.getData().getUsers())users.add(u);
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
        }
    }
    
    String getMD5(String s) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashInBytes = md.digest(s.getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) sb.append(String.format("%02x", b));
        
        return sb.toString();
    }
    
    public void addUser() throws IOException, NoSuchAlgorithmException{
        conex conn = new conex();
        
        reqUser re = new reqUser();
        reqUser.Data data = re.new Data(new user(id_profile, name, user, getMD5(passwd)));
        
        req = new reqUser(new Auth(((login)session.getObject("user")).getUser(),"users","new"), data);
        res = (resUser) conn.sendJSON(req, resUser.class);
        
        if(res.getError().getCode().equals("")){
            findUsers();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Agregado usuario "+name));
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
        }
    }
    
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edición cancelada", ((user) event.getObject()).getUser());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void updUser(RowEditEvent event) throws IOException, NoSuchAlgorithmException{
        conex conn = new conex();
        
        reqUser re = new reqUser();
        reqUser.Data data = re.new Data(new user(((user) event.getObject()).getId(), ((user) event.getObject()).getId_profile(), ((user) event.getObject()).getName(), ((user) event.getObject()).getUser(), getMD5(((user) event.getObject()).getPasswd())));
        
        req = new reqUser(new Auth(((login)session.getObject("user")).getUser(),"users","edit"), data);
        res = (resUser) conn.sendJSON(req, resUser.class);
        
        if(res.getError().getCode().equals("")){
            findUsers();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Actualizado usuario "+((user) event.getObject()).getUser()));
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
        }
    }
    
    public void delUser(int id, String _user) throws IOException{
        conex conn = new conex();
        
        reqUser re = new reqUser();
        reqUser.Data data = re.new Data(new user(id, 0, null, _user, null));
        
        req = new reqUser(new Auth(((login)session.getObject("user")).getUser(),"users","delete"), data);
        res = (resUser) conn.sendJSON(req, resUser.class);
        
        if(res.getError().getCode().equals("")){
            findUsers();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Eliminado usuario "+_user));
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
        }
    }

    public int getId_profile() {
        return id_profile;
    }

    public void setId_profile(int id_profile) {
        this.id_profile = id_profile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public List<profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<profile> profiles) {
        this.profiles = profiles;
    }

    public List<user> getUsers() {
        return users;
    }

    public void setUsers(List<user> users) {
        this.users = users;
    }
}
