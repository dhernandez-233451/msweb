/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.profiles;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.beans.profiles.module;
import mx.com.wwn.migrasap.beans.profiles.profile;
import mx.com.wwn.migrasap.beans.profiles.reqModule;
import mx.com.wwn.migrasap.beans.profiles.resModule;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.session;
/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlModules")
public class ctrlModules {
    private profile _profile;
    private List<module> modules;
    
    reqModule req;
    resModule res;
    
    public ctrlModules() throws IOException{
        _profile = (profile) session.getObject("profile_edit");
        //session.removeObject("profile_edit");
        findModules();
    }
    
    public void findModules() throws IOException{
        conex conn = new conex();
        modules = new ArrayList<>();
        
        reqModule re = new reqModule();
        reqModule.Data data = re.new Data(_profile, null);
        
        req = new reqModule(new Auth(((login)session.getObject("user")).getUser(),"modules","find"), data);
        res = (resModule) conn.sendJSON(req, resModule.class);
        
        if(res.getError().getCode().equals("")){
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),""));
            for(module m : res.getData().getModules())modules.add(m);
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
        }
    }
    
    public void updModules() throws IOException{
        conex conn = new conex();
        
        reqModule re = new reqModule();
        reqModule.Data data = re.new Data(_profile, modules);
        
        req = new reqModule(new Auth(((login)session.getObject("user")).getUser(),"modules","edit"), data);
        res = (resModule) conn.sendJSON(req, resModule.class);
        
        if(res.getError().getCode().equals("")){
            findModules();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Actualizado perfil "+_profile.getName()));
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
        }
    }

    public List<module> getModules() {
        return modules;
    }

    public void setModules(List<module> modules) {
        this.modules = modules;
    }
}
