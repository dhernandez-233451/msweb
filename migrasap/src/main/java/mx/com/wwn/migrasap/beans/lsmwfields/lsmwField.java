/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.lsmwfields;

/**
 *
 * @author daniel
 */
public class lsmwField {
    private int id;
    private int id_lsmw;
    private String lsmw;
    private String field;
    
    public lsmwField(){
        super();
    }

    public lsmwField(int id_lsmw) {
        this.id_lsmw = id_lsmw;
    }

    public lsmwField(int id_lsmw, String field) {
        this.id_lsmw = id_lsmw;
        this.field = field;
    }

    public lsmwField(int id, int id_lsmw, String lsmw, String field) {
        this.id = id;
        this.id_lsmw = id_lsmw;
        this.lsmw = lsmw;
        this.field = field;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_lsmw() {
        return id_lsmw;
    }

    public void setId_lsmw(int id_lsmw) {
        this.id_lsmw = id_lsmw;
    }

    public String getLsmw() {
        return lsmw;
    }

    public void setLsmw(String lsmw) {
        this.lsmw = lsmw;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
