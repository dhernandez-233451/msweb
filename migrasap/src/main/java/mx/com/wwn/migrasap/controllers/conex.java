/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author daniel
 */
public class conex {
    private String url;
    private static String protocol = "http",
            ip = "127.0.0.1",
            port = "80",
            directory = "/migrasap/controllers/",
            file = "main.php";
    
    public conex(){
        url = protocol+"://"+ip+":"+port+directory+file;
        System.out.println("URL: "+this.url);
    }
    
    String objToJSON(Object obj) throws JsonProcessingException{
        ObjectMapper om = new ObjectMapper();
        String json = om.writeValueAsString(obj);
        System.out.println("REQ: "+json);
        return json;
    }
    
    Object jsonToObj(String res, Class cls) throws IOException{
        //res = om.readValue(response.toString(), response.class);
        ObjectMapper om = new ObjectMapper();
        System.out.println("RES: "+res);
        return om.readValue(res, cls);
    }
    
    public Object sendJSON(Object obj, Class cls) throws MalformedURLException, IOException{
        String json = objToJSON(obj);
        
        URL url = new URL(this.url);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        
        OutputStream os = conn.getOutputStream();
        os.write(json.getBytes("UTF-8"));
        os.close();
        
        String result;
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuffer response = new StringBuffer();
        while((result = in.readLine())!= null){
            response.append(result);
        }
        in.close();
        conn.disconnect();
        //System.out.println("Serv: "+response.toString());
        return jsonToObj(response.toString(), cls);
    }
}
