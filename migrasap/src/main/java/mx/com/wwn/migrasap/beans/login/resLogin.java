/*

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.login;

import mx.com.wwn.migrasap.beans.Error;

/**
 *
 * @author daniel
 */
public class resLogin {
    private Error error;
    private Data data;
    
    public resLogin(){
        super();
    }
    
    public resLogin(Error error, Data data){
        this.error = error;
        this.data = data;
    }
    
    public class Data{
        private login login;
        
        public Data(){
            super();
        }
        
        public Data(login login){
            this.login = login;
        }

        public login getLogin() {
            return login;
        }

        public void setLogin(login login) {
            this.login = login;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
