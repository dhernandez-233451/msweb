/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.rules;

import java.util.List;
import mx.com.wwn.migrasap.beans.Auth;

/**
 *
 * @author daniel
 */
public class reqRule {
    private Auth auth;
    private Data data;
    
    public reqRule(){
        super();
    }

    public reqRule(Auth auth, Data data) {
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private List<rule> rules;
        
        public Data(){
            super();
        }

        public Data(List<rule> rules) {
            this.rules = rules;
        }

        public List<rule> getRules() {
            return rules;
        }

        public void setRules(List<rule> rules) {
            this.rules = rules;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
