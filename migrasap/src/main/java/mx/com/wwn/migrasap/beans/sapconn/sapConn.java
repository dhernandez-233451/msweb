/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.sapconn;

/**
 *
 * @author daniel
 */
public class sapConn {
    private int id;
    private String name;
    private String host;
    private String sysnr;
    private String client;
    private String user;
    private String passwd;
    private String lang;
    private String saprouter;
    private String function;
    
    public sapConn(){
        super();
    }

    public sapConn(String name, String host, String sysnr, String client, String user, String passwd, String lang, String saprouter, String function) {
        this.name = name;
        this.host = host;
        this.sysnr = sysnr;
        this.client = client;
        this.user = user;
        this.passwd = passwd;
        this.lang = lang;
        this.saprouter = saprouter;
        this.function = function;
    }

    public sapConn(int id, String name, String host, String sysnr, String client, String user, String passwd, String lang, String saprouter, String function) {
        this.id = id;
        this.name = name;
        this.host = host;
        this.sysnr = sysnr;
        this.client = client;
        this.user = user;
        this.passwd = passwd;
        this.lang = lang;
        this.saprouter = saprouter;
        this.function = function;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getSysnr() {
        return sysnr;
    }

    public void setSysnr(String sysnr) {
        this.sysnr = sysnr;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getSaprouter() {
        return saprouter;
    }

    public void setSaprouter(String saprouter) {
        this.saprouter = saprouter;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }
}