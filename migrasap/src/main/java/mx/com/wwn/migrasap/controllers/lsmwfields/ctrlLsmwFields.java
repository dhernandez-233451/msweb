/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.lsmwfields;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.beans.lsmwfields.*;
import mx.com.wwn.migrasap.beans.lsmws.*;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.session;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlLsmwFields")
public class ctrlLsmwFields {
    private int id;
    private static int id_lsmw;
    private String lsmw;
    private String field;
    private List<lsmw> lsmws;
    private List<lsmwField> lsmwFields;
    
    reqLsmwField req;
    resLsmwField res;
    
    public ctrlLsmwFields() throws IOException{
        findLsmws();
        findLsmwFields();
    }
    
    public void findLsmws() throws IOException{
        conex conn = new conex();
        lsmws = new ArrayList<>();
        
        reqLsmw re = new reqLsmw();
        reqLsmw.Data data = re.new Data(new lsmw());
        
        reqLsmw req0 = new reqLsmw(new Auth(((login)session.getObject("user")).getUser(),"lsmws","find"), data);
        resLsmw res0 = (resLsmw) conn.sendJSON(req0, resLsmw.class);
        
        if(res0.getError().getCode().equals(""))for(lsmw l : res0.getData().getLsmws())lsmws.add(l);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res0.getError().getCode(), res0.getError().getMessage()));
    }
    
    public void findLsmwFields() throws IOException{
        conex conn = new conex();
        lsmwFields = new ArrayList<>();
        
        reqLsmwField re = new reqLsmwField();
        reqLsmwField.Data data = re.new Data(new lsmwField(id_lsmw));
        
        req = new reqLsmwField(new Auth(((login)session.getObject("user")).getUser(), "lsmwfields", "find"), data);
        res = (resLsmwField) conn.sendJSON(req, resLsmwField.class);
        
        if(res.getError().getCode().equals(""))for(lsmwField lf : res.getData().getLsmwFields())lsmwFields.add(lf);
        //else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void addLsmwField() throws IOException{
        conex conn = new conex();
        
        reqLsmwField re = new reqLsmwField();
        reqLsmwField.Data data = re.new Data(new lsmwField(id_lsmw, field));
        
        req = new reqLsmwField(new Auth(((login)session.getObject("user")).getUser(), "lsmwfields", "new"), data);
        res = (resLsmwField) conn.sendJSON(req, resLsmwField.class);
        
        if(res.getError().getCode().equals("")){
            findLsmwFields();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Agregado campo "+field));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edición cancelada", ((lsmwField) event.getObject()).getField());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void updLsmwField(RowEditEvent event) throws IOException{
        conex conn = new conex();
        
        reqLsmwField re = new reqLsmwField();
        reqLsmwField.Data data = re.new Data(new lsmwField(((lsmwField) event.getObject()).getId(), ((lsmwField) event.getObject()).getField()));
        
        req = new reqLsmwField(new Auth(((login)session.getObject("user")).getUser(), "lsmwfields", "edit"), data);
        res = (resLsmwField) conn.sendJSON(req, resLsmwField.class);
        
        if(res.getError().getCode().equals("")){
            findLsmwFields();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Actualziado campo "+((lsmwField) event.getObject()).getField()));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void delLsmwField(int id, String field) throws IOException{
        conex conn = new conex();
        
        reqLsmwField re = new reqLsmwField();
        reqLsmwField.Data data = re.new Data(new lsmwField(id));
        
        req = new reqLsmwField(new Auth(((login)session.getObject("user")).getUser(), "lsmwfields", "delete"), data);
        res = (resLsmwField) conn.sendJSON(req, resLsmwField.class);
        
        if(res.getError().getCode().equals("")){
            findLsmwFields();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Eliminado campo "+field));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_lsmw() {
        return id_lsmw;
    }

    public void setId_lsmw(int id_lsmw) {
        ctrlLsmwFields.id_lsmw = id_lsmw;
    }

    public String getLsmw() {
        return lsmw;
    }

    public void setLsmw(String lsmw) {
        this.lsmw = lsmw;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public List<lsmw> getLsmws() {
        return lsmws;
    }

    public void setLsmws(List<lsmw> lsmws) {
        this.lsmws = lsmws;
    }

    public List<lsmwField> getLsmwFields() {
        return lsmwFields;
    }

    public void setLsmwFields(List<lsmwField> lsmwFields) {
        this.lsmwFields = lsmwFields;
    }
}
