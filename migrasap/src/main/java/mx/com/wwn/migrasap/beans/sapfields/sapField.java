/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.sapfields;

/**
 *
 * @author daniel
 */
public class sapField {
    private int id;
    private int id_sap_table;
    private String sap_table;
    private String name;
    
    public sapField(){
        super();
    }

    public sapField(int id_sap_table) {
        this.id_sap_table = id_sap_table;
    }

    public sapField(int id_sap_table, String name) {
        this.id_sap_table = id_sap_table;
        this.name = name;
    }

    public sapField(int id, int id_sap_table, String sap_table, String name) {
        this.id = id;
        this.id_sap_table = id_sap_table;
        this.sap_table = sap_table;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_sap_table() {
        return id_sap_table;
    }

    public void setId_sap_table(int id_sap_table) {
        this.id_sap_table = id_sap_table;
    }

    public String getSap_table() {
        return sap_table;
    }

    public void setSap_table(String sap_table) {
        this.sap_table = sap_table;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
