/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.mapping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.beans.lsmws.*;
import mx.com.wwn.migrasap.beans.mapping.*;
import mx.com.wwn.migrasap.beans.sapfields.*;
import mx.com.wwn.migrasap.beans.saptables.*;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.session;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlMapping")
public class ctrlMapping {    
    private static int id_lsmw;
    private String lsmw;
    private int id_lsmw_field;
    private String lsmw_field;
    private static int id_sap_table;
    private String sap_table;
    private int id_sap_field;
    private String sap_field;
    
    private List<lsmw> lsmws;
    private static List<mapping> mapping;
    private List<sapTable> sapTables;
    private List<sapField> sapFields;
    
    reqMapping req;
    resMapping res;
    
    public ctrlMapping() throws IOException{
        findLsmws();
        findSapTables();
        findSapFields(id_sap_table);
        //findMapping();  
    }
    
    public void findLsmws() throws IOException{
        conex conn = new conex();
        lsmws = new ArrayList<>();
        
        reqLsmw re = new reqLsmw();
        reqLsmw.Data data = re.new Data(new lsmw());
        
        reqLsmw req0 = new reqLsmw(new Auth(((login)session.getObject("user")).getUser(),"lsmws","find"), data);
        resLsmw res0 = (resLsmw) conn.sendJSON(req0, resLsmw.class);
        
        if(res0.getError().getCode().equals(""))for(lsmw l : res0.getData().getLsmws())lsmws.add(l);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res0.getError().getCode(), res0.getError().getMessage()));
    }
    
    public void findMapping() throws IOException{
        conex conn = new conex();
        mapping = new ArrayList<>();
        
        reqMapping re = new reqMapping();
        reqMapping.Data data = re.new Data(id_lsmw);
        
        req = new reqMapping(new Auth(((login)session.getObject("user")).getUser(),"mapping","find"), data);
        res = (resMapping) conn.sendJSON(req, resMapping.class);
        
        if(res.getError().getCode().equals(""))for(mapping m : res.getData().getMapping())mapping.add(m);
        //else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void findSapTables() throws IOException{
        conex conn = new conex();
        sapTables = new ArrayList<>();
        
        reqSapTable re = new reqSapTable();
        reqSapTable.Data data = re.new Data(new sapTable());
        
        reqSapTable req0 = new reqSapTable(new Auth(((login)session.getObject("user")).getUser(),"saptables","find"), data);
        resSapTable res0 = (resSapTable) conn.sendJSON(req0, resSapTable.class);
        
        if(res0.getError().getCode().equals(""))for(sapTable s : res0.getData().getSapTables())sapTables.add(s);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res0.getError().getCode(), res0.getError().getMessage()));
    }
    
    public void findSapFields(int ist) throws IOException{
        conex conn = new conex();
        sapFields = new ArrayList<>();
        id_sap_table = ist;
        
        reqSapField re = new reqSapField();
        reqSapField.Data data = re.new Data(new sapField(ist));
        
        reqSapField req0 = new reqSapField(new Auth(((login)session.getObject("user")).getUser(), "sapfields", "find"), data);
        resSapField res0 = (resSapField) conn.sendJSON(req0, resSapField.class);
        
        if(res0.getError().getCode().equals(""))for(sapField s : res0.getData().getSapFields())sapFields.add(s);
        //else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res0.getError().getCode(), res0.getError().getMessage()));
    }
    
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edición cancelada", ((mapping) event.getObject()).getLsmw_field());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void updMapping(RowEditEvent event) throws IOException{
        conex conn = new conex();
        
        mapping m = new mapping(((mapping) event.getObject()).getId_lsmw(), ((mapping) event.getObject()).getLsmw(), ((mapping) event.getObject()).getId_lsmw_field(), ((mapping) event.getObject()).getLsmw_field(), ((mapping) event.getObject()).getId_sap_table(), ((mapping) event.getObject()).getSap_table(), ((mapping) event.getObject()).getId_sap_field(), ((mapping) event.getObject()).getSap_field());
        List<mapping> l = new ArrayList<>();
        l.add(m);        
        
        reqMapping re = new reqMapping();
        reqMapping.Data data = re.new Data(l);
        
        req = new reqMapping(new Auth(((login)session.getObject("user")).getUser(),"mapping","edit"), data);
        res = (resMapping) conn.sendJSON(req, resMapping.class);
        
        if(res.getError().getCode().equals("")){            
            findMapping();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Actualizado mapeo"));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }

    public int getId_lsmw() {
        return id_lsmw;
    }

    public void setId_lsmw(int id_lsmw) {
        this.id_lsmw = id_lsmw;
    }

    public String getLsmw() {
        return lsmw;
    }

    public void setLsmw(String lsmw) {
        this.lsmw = lsmw;
    }

    public int getId_lsmw_field() {
        return id_lsmw_field;
    }

    public void setId_lsmw_field(int id_lsmw_field) {
        this.id_lsmw_field = id_lsmw_field;
    }

    public String getLsmw_field() {
        return lsmw_field;
    }

    public void setLsmw_field(String lsmw_field) {
        this.lsmw_field = lsmw_field;
    }

    public int getId_sap_table() {
        return id_sap_table;
    }

    public void setId_sap_table(int id_sap_table) {
        this.id_sap_table = id_sap_table;
    }

    public String getSap_table() {
        return sap_table;
    }

    public void setSap_table(String sap_table) {
        this.sap_table = sap_table;
    }

    public int getId_sap_field() {
        return id_sap_field;
    }

    public void setId_sap_field(int id_sap_field) {
        this.id_sap_field = id_sap_field;
    }

    public String getSap_field() {
        return sap_field;
    }

    public void setSap_field(String sap_field) {
        this.sap_field = sap_field;
    }

    public List<lsmw> getLsmws() {
        return lsmws;
    }

    public void setLsmws(List<lsmw> lsmws) {
        this.lsmws = lsmws;
    }

    public List<mapping> getMapping() {
        return mapping;
    }

    public void setMapping(List<mapping> mapping) {
        this.mapping = mapping;
    }

    public List<sapTable> getSapTables() {
        return sapTables;
    }

    public void setSapTables(List<sapTable> sapTables) {
        this.sapTables = sapTables;
    }

    public List<sapField> getSapFields() {
        return sapFields;
    }

    public void setSapFields(List<sapField> sapFields) {
        this.sapFields = sapFields;
    }
}