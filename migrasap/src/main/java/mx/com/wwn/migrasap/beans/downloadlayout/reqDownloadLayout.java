/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.downloadlayout;

import mx.com.wwn.migrasap.beans.Auth;

/**
 *
 * @author daniel
 */
public class reqDownloadLayout {
    private Auth auth;
    private Data data;

    public reqDownloadLayout() {
        super();
    }

    public reqDownloadLayout(Auth auth, Data data) {
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private downloadLayout downloadLayout;

        public Data() {
            super();
        }

        public Data(downloadLayout downloadLayout) {
            this.downloadLayout = downloadLayout;
        }

        public downloadLayout getDownloadLayout() {
            return downloadLayout;
        }

        public void setDownloadLayout(downloadLayout downloadLayout) {
            this.downloadLayout = downloadLayout;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
