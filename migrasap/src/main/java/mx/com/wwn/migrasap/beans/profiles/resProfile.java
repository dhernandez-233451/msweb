/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.profiles;

import java.util.List;
import mx.com.wwn.migrasap.beans.Error;
/**
 *
 * @author daniel
 */
public class resProfile {
    private Error error;
    private Data data;
    
    public resProfile(){
        super();
    }
    
    public resProfile(Error error, Data data){
        this.error = error;
        this.data = data;
    }
    
    public class Data{
        private List<profile> profiles;
        
        public Data(){
            super();
        }
        
        public Data(List<profile> profiles){
            this.profiles = profiles;
        }

        public List<profile> getProfiles() {
            return profiles;
        }

        public void setProfiles(List<profile> profiles) {
            this.profiles = profiles;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
