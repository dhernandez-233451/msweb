/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.downloadlayout;

import java.util.List;
import mx.com.wwn.migrasap.beans.Error;
/**
 *
 * @author daniel
 */
public class resDownloadLayout {
    private Error error;
    private Data data;

    public resDownloadLayout() {
        super();
    }

    public resDownloadLayout(Error error, Data data) {
        this.error = error;
        this.data = data;
    }
    
    public class Data{
        private downloadLayout downloadLayout;

        public Data() {
            super();
        }

        public Data(downloadLayout downloadLayout) {
            this.downloadLayout = downloadLayout;
        }

        public downloadLayout getDownloadLayout() {
            return downloadLayout;
        }

        public void setDownloadLayout(downloadLayout downloadLayout) {
            this.downloadLayout = downloadLayout;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
