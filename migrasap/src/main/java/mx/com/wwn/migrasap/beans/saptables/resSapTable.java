/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.saptables;

import java.util.List;
import mx.com.wwn.migrasap.beans.Error;
/**
 *
 * @author daniel
 */
public class resSapTable {
    private Error error;
    private Data data;
    
    public resSapTable(){
        super();
    }

    public resSapTable(Error error, Data data) {
        this.error = error;
        this.data = data;
    }
    
    public class Data{
        private List<sapTable> sapTables;
        
        public Data(){
            super();
        }

        public Data(List<sapTable> sapTables) {
            this.sapTables = sapTables;
        }

        public List<sapTable> getSapTables() {
            return sapTables;
        }

        public void setSapTables(List<sapTable> sapTables) {
            this.sapTables = sapTables;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
