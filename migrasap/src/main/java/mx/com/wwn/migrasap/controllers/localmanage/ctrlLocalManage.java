/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.localmanage;

import com.sap.conn.jco.JCoException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.Error;
import mx.com.wwn.migrasap.beans.localmanage.*;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.beans.sapconn.*;
import mx.com.wwn.migrasap.beans.sapfields.*;
import mx.com.wwn.migrasap.beans.saptables.*;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.session;
import mx.com.wwn.migrasap.controllers.files;
import mx.com.wwn.migrasap.controllers.rfc.RFC;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlLocalManage")
public class ctrlLocalManage {
    private StreamedContent file;
    private byte[] filebytes;
    private static int idsc;
    private static String passwd;
    private List<sapConn> sapConns;
    
    List<sapTable> sapTables;
    List<sapField> sapFields;
    List<String> fields;
    List<String[]> data;
    
    reqLocalManage reqLM;
    resLocalManage resLM;
    reqMassiveImport reqMI;
    resMassiveImport resMI;
    
    public ctrlLocalManage() throws IOException {
        findSapConns();
    }
    
    sapConn findSapConn(int id){
        sapConn sc = null;
        for(sapConn s : sapConns)if(s.getId() == id){
                sc = s;
                break;
            }
        return sc;
    }
    
    public void findSapConns() throws IOException{
        conex conn = new conex();
        sapConns = new ArrayList<>();
        
        reqSapConn re = new reqSapConn();
        reqSapConn.Data data = re.new Data(new sapConn());
        
        reqSapConn req0 = new reqSapConn(new Auth(((login) session.getObject("user")).getUser(),"sapconn","find"), data);
        resSapConn res0 = (resSapConn) conn.sendJSON(req0, resSapConn.class);
        
        if(res0.getError().getCode().equals(""))for(sapConn s : res0.getData().getSapConns()) sapConns.add(s);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res0.getError().getCode(), res0.getError().getMessage()));
    }
    
    public StreamedContent backup(String dbname) throws IOException{
        conex conn = new conex();
        files f = new files();
        
        reqLocalManage re = new reqLocalManage();
        reqLocalManage.Data data = re.new Data(new localManage(dbname));
        
        reqLM = new reqLocalManage(new Auth(((login) session.getObject("user")).getUser(), "localmanage", "backup"), data);
        resLM = (resLocalManage) conn.sendJSON(reqLM, resLocalManage.class);
        
        if(resLM.getError().getCode().equals("")){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Excelente", dbname));
            return f.convertToFile(resLM.getData().getLocalManage().getDbname(), resLM.getData().getLocalManage().getContent());
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+resLM.getError().getCode(), resLM.getError().getMessage()));
            return null;
        }
    }
    
    public void restore(FileUploadEvent event) throws IOException{
        conex conn = new conex();
        files f = new files();
        
        reqLocalManage re = new reqLocalManage();
        reqLocalManage.Data data = re.new Data(new localManage((String) event.getComponent().getAttributes().get("db"), f.convertToBase64(event.getFile())));
        
        reqLM = new reqLocalManage(new Auth(((login) session.getObject("user")).getUser(), "localmanage", "restore"),data);
        resLM = (resLocalManage) conn.sendJSON(reqLM, resLocalManage.class);
        
        if(resLM.getError().getCode().equals("")) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Restaurada base", (String) event.getComponent().getAttributes().get("db")));
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+resLM.getError().getCode(), resLM.getError().getMessage()));
    }
    
    public void clean(String dbname) throws IOException{
        conex conn = new conex();
        
        reqLocalManage re = new reqLocalManage();
        reqLocalManage.Data data = re.new Data(new localManage(dbname));
        
        reqLM = new reqLocalManage(new Auth(((login) session.getObject("user")).getUser(), "localmanage", "clean"),data);
        resLM = (resLocalManage) conn.sendJSON(reqLM, resLocalManage.class);
        
        if(resLM.getError().getCode().equals("")) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Depurada base", dbname));
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+resLM.getError().getCode(), resLM.getError().getMessage()));
    }
    
    public void findSapTables() throws IOException{
        conex conn = new conex();
        sapTables = new ArrayList<>();
        
        reqSapTable re = new reqSapTable();
        reqSapTable.Data data = re.new Data(new sapTable());
        
        reqSapTable req0 = new reqSapTable(new Auth(((login)session.getObject("user")).getUser(),"saptables","find"), data);
        resSapTable res0 = (resSapTable) conn.sendJSON(req0, resSapTable.class);
        
        if(res0.getError().getCode().equals(""))for(sapTable s : res0.getData().getSapTables())sapTables.add(s);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res0.getError().getCode(), res0.getError().getMessage()));
    }
    
    public void findSapFields(int id_sap_table) throws IOException{
        conex conn = new conex();
        sapFields = new ArrayList<>();
        
        reqSapField re = new reqSapField();
        reqSapField.Data data = re.new Data(new sapField(id_sap_table));
        
        reqSapField req0 = new reqSapField(new Auth(((login)session.getObject("user")).getUser(), "sapfields", "find"), data);
        resSapField res0 = (resSapField) conn.sendJSON(req0, resSapField.class);
        
        if(res0.getError().getCode().equals(""))for(sapField s : res0.getData().getSapFields())sapFields.add(s);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res0.getError().getCode(), res0.getError().getMessage()));
    }
    
    void massiveLocalImport(List<massiveImport> mi, String connName) throws IOException{
        conex conn = new conex();
        
        reqMassiveImport re = new reqMassiveImport();
        reqMassiveImport.Data data = re.new Data(mi);
        
        reqMI = new reqMassiveImport(new Auth(((login)session.getObject("user")).getUser(), "localmanage", "import"), data);
        resMI = (resMassiveImport) conn.sendJSON(reqMI, resMassiveImport.class);
        
        if(resMI.getError().getCode().equals("")) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Importación exitosa "+connName, resMI.getError().getMessage()));
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+resMI.getError().getCode(), resMI.getError().getMessage()));
    }
    
    boolean isEmptyDB(String db) throws IOException{
        conex conn = new conex();
        
        reqLocalManage re = new reqLocalManage();
        reqLocalManage.Data data = re.new Data(new localManage(db));
        
        reqLM = new reqLocalManage(new Auth(((login) session.getObject("user")).getUser(), "localmanage", "isempty"),data);
        resLM = (resLocalManage) conn.sendJSON(reqLM, resLocalManage.class);
        
        return (resLM.getError().getCode().equals(""))? true : false;
    }
    
    public void massiveImport() throws IOException, JCoException{
        if(idsc != 0){
            if(isEmptyDB("msmirror")){
                List<massiveImport> mImport = new ArrayList<massiveImport>();
                List<String> fieldsName;
                RFC rfc;
                sapConn sc = findSapConn(idsc);

                findSapTables();                    //Traer lista de tablas SAP
                for(sapTable t : sapTables){
                    fieldsName = new ArrayList<String>();
                    findSapFields(t.getId());       //Traer lista de campos por tablas SAP                
                    for(sapField f : sapFields) fieldsName.add(f.getName());

                    rfc = new RFC();                //Preparar jco para cada tabla con sus respectivos campos
                    rfc.setConnectionParameters(sc.getHost(), sc.getSysnr(), sc.getClient(), sc.getUser(), passwd, sc.getLang(), sc.getSaprouter(), sc.getFunction());
                    rfc.extractData(t.getName(), fieldsName);
                    mImport.add(new massiveImport(t.getId(), t.getName(), rfc.fields, rfc.data));
                }
                idsc = 0;
                passwd = "";
                massiveLocalImport(mImport, sc.getName());        //Enviar JSON de datos a PHP
            }
            else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "La base espejo ya tiene datos SAP"));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "No se ha elegido el servidor SAP"));
    }
    
    public void massiveTransform() throws IOException{
        if(isEmptyDB("mshandling")){
            conex conn = new conex();
        
            reqLocalManage re = new reqLocalManage();
            reqLocalManage.Data data = re.new Data();

            reqLM = new reqLocalManage(new Auth(((login) session.getObject("user")).getUser(), "localmanage", "transform"), data);
            resLM = (resLocalManage) conn.sendJSON(reqLM, resLocalManage.class);

            if(resLM.getError().getCode().equals("")) FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Transformación exitosa", resLM.getError().getMessage()));
            else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+resLM.getError().getCode(), resLM.getError().getMessage()));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "La base de manipulación ya tiene datos"));
    }

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public int getIdsc() {
        return idsc;
    }

    public void setIdsc(int idsc) {
        this.idsc = idsc;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public List<sapConn> getSapConns() {
        return sapConns;
    }

    public void setSapConns(List<sapConn> sapConns) {
        this.sapConns = sapConns;
    }
}
