/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.localmanage;

import java.util.List;

/**
 *
 * @author daniel
 */
public class massiveImport {
    private int idTable;
    private String table;
    private List<String> fields;
    private List<String[]> data;

    public massiveImport() {
        super();
    }

    public massiveImport(int idTable, String table, List<String> fields, List<String[]> data) {
        this.idTable = idTable;
        this.table = table;
        this.fields = fields;
        this.data = data;
    }

    public int getIdTable() {
        return idTable;
    }

    public void setIdTable(int idTable) {
        this.idTable = idTable;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public List<String[]> getData() {
        return data;
    }

    public void setData(List<String[]> data) {
        this.data = data;
    }
}
