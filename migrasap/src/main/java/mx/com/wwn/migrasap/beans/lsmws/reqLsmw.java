/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.lsmws;

import mx.com.wwn.migrasap.beans.Auth;

/**
 *
 * @author daniel
 */
public class reqLsmw {
    private Auth auth;
    private Data data;
    
    public reqLsmw(){
        super();
    }

    public reqLsmw(Auth auth, Data data) {
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private lsmw lsmw;
        
        public Data(){
            super();
        }

        public Data(lsmw lsmw) {
            this.lsmw = lsmw;
        }

        public lsmw getLsmw() {
            return lsmw;
        }

        public void setLsmwLy(lsmw lsmw) {
            this.lsmw = lsmw;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
