/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.profiles;

import mx.com.wwn.migrasap.beans.Auth;

/**
 *
 * @author daniel
 */
public class reqProfile {
    private Auth auth;
    private Data data;
    
    public reqProfile(){
        super();
    }
    
    public reqProfile(Auth auth, Data data){
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private profile profile;
        
        public Data(){
            super();
        }
        
        public Data(profile profile){
            this.profile = profile;
        }

        public profile getProfile() {
            return profile;
        }

        public void setProfile(profile profile) {
            this.profile = profile;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
