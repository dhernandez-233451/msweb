/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.main;

import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.controllers.session;
/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlMain")
public class ctrlMain {
    private static String name;
    
    public ctrlMain(){
        /*HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        login login = (login) session.getAttribute("user");*/
        this.name = ((login) session.getObject("user")).getName();
    }
    
    public boolean showElement(String elements){
        boolean permit = false;
        for(String s : elements.split(","))for(int i : ((login)session.getObject("user")).getPermits())if(Integer.parseInt(s) == i){
            permit = true;
            break;
        }
        return permit;
    }
    
    public String logout(){
        session.endSession();
        return "/index.xhtml";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
