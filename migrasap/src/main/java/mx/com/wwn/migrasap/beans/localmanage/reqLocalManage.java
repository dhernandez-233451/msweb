/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.localmanage;

import mx.com.wwn.migrasap.beans.Auth;

/**
 *
 * @author daniel
 */
public class reqLocalManage {
    private Auth auth;
    private Data data;
    
    public reqLocalManage(){
        super();
    }

    public reqLocalManage(Auth auth, Data data) {
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private localManage localManage;
        
        public Data(){
            super();
        }

        public Data(localManage localManage) {
            this.localManage = localManage;
        }

        public localManage getLocalManage() {
            return localManage;
        }

        public void setLocalManage(localManage localManage) {
            this.localManage = localManage;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}