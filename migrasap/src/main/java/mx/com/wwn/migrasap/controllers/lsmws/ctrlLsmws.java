/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers.lsmws;

import mx.com.wwn.migrasap.beans.lsmws.resLsmw;
import mx.com.wwn.migrasap.beans.lsmws.reqLsmw;
import mx.com.wwn.migrasap.beans.lsmws.lsmw;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import mx.com.wwn.migrasap.beans.Auth;
import mx.com.wwn.migrasap.beans.login.login;
import mx.com.wwn.migrasap.controllers.conex;
import mx.com.wwn.migrasap.controllers.session;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author daniel
 */
@ManagedBean
@RequestScoped
@Named("ctrlLsmwLys")
public class ctrlLsmws {
    private int id;
    private String title;
    private List<lsmw> lsmws;
    reqLsmw req;
    resLsmw res;
    
    public ctrlLsmws() throws IOException{
        findLsmws();
    }   
    
    
    public void findLsmws() throws IOException{
        conex conn = new conex();
        lsmws = new ArrayList<>();
        
        reqLsmw re = new reqLsmw();
        reqLsmw.Data data = re.new Data(new lsmw());
        
        req = new reqLsmw(new Auth(((login)session.getObject("user")).getUser(),"lsmws","find"), data);
        res = (resLsmw) conn.sendJSON(req, resLsmw.class);
        
        if(res.getError().getCode().equals(""))for(lsmw l : res.getData().getLsmws())lsmws.add(l);
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void addLsmw() throws IOException{
        conex conn = new conex();
        
        reqLsmw re = new reqLsmw();
        reqLsmw.Data data = re.new Data(new lsmw(title));
        
        req = new reqLsmw(new Auth(((login)session.getObject("user")).getUser(),"lsmws","new"), data);
        res = (resLsmw) conn.sendJSON(req, resLsmw.class);
        
        if(res.getError().getCode().equals("")){
            findLsmws();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Agregado Layout "+title));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edición cancelada", ((lsmw) event.getObject()).getTitle());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void updLsmw(RowEditEvent event) throws IOException{
        conex conn = new conex();
        
        reqLsmw re = new reqLsmw();
        reqLsmw.Data data = re.new Data(new lsmw(((lsmw) event.getObject()).getId(), ((lsmw) event.getObject()).getTitle()));
        
        req = new reqLsmw(new Auth(((login)session.getObject("user")).getUser(),"lsmws","edit"), data);
        res = (resLsmw) conn.sendJSON(req, resLsmw.class);
        
        if(res.getError().getCode().equals("")){
            findLsmws();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Actualizado Layout "+((lsmw) event.getObject()).getTitle()));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }
    
    public void delLsmw(int id, String title) throws IOException{
        conex conn = new conex();
        
        reqLsmw re = new reqLsmw();
        reqLsmw.Data data = re.new Data(new lsmw(id, title));
        
        req = new reqLsmw(new Auth(((login)session.getObject("user")).getUser(),"lsmws","delete"), data);
        res = (resLsmw) conn.sendJSON(req, resLsmw.class);
        
        if(res.getError().getCode().equals("")){
            findLsmws();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, res.getError().getMessage(),"Eliminado Layout "+title));
        }
        else FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<lsmw> getLsmws() {
        return lsmws;
    }

    public void setLsmws(List<lsmw> lsmws) {
        this.lsmws = lsmws;
    }
}
