/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.mapping;

/**
 *
 * @author daniel
 */
public class mapping {
    private int id_lsmw;
    private String lsmw;
    private int id_lsmw_field;
    private String lsmw_field;
    private int id_sap_table;
    private String sap_table;
    private int id_sap_field;
    private String sap_field;
    
    public mapping(){
        super();
    }

    public mapping(int id_lsmw, String lsmw, int id_lsmw_field, String lsmw_field, int id_sap_table, String sap_table, int id_sap_field, String sap_field) {
        this.id_lsmw = id_lsmw;
        this.lsmw = lsmw;
        this.id_lsmw_field = id_lsmw_field;
        this.lsmw_field = lsmw_field;
        this.id_sap_table = id_sap_table;
        this.sap_table = sap_table;
        this.id_sap_field = id_sap_field;
        this.sap_field = sap_field;
    }

    public int getId_lsmw() {
        return id_lsmw;
    }

    public void setId_lsmw(int id_lsmw) {
        this.id_lsmw = id_lsmw;
    }

    public String getLsmw() {
        return lsmw;
    }

    public void setLsmw(String lsmw) {
        this.lsmw = lsmw;
    }

    public int getId_lsmw_field() {
        return id_lsmw_field;
    }

    public void setId_lsmw_field(int id_lsmw_field) {
        this.id_lsmw_field = id_lsmw_field;
    }

    public String getLsmw_field() {
        return lsmw_field;
    }

    public void setLsmw_field(String lsmw_field) {
        this.lsmw_field = lsmw_field;
    }

    public int getId_sap_table() {
        return id_sap_table;
    }

    public void setId_sap_table(int id_sap_table) {
        this.id_sap_table = id_sap_table;
    }

    public String getSap_table() {
        return sap_table;
    }

    public void setSap_table(String sap_table) {
        this.sap_table = sap_table;
    }

    public int getId_sap_field() {
        return id_sap_field;
    }

    public void setId_sap_field(int id_sap_field) {
        this.id_sap_field = id_sap_field;
    }

    public String getSap_field() {
        return sap_field;
    }

    public void setSap_field(String sap_field) {
        this.sap_field = sap_field;
    }
}
