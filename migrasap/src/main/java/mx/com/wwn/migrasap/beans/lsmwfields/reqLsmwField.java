/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.lsmwfields;

import mx.com.wwn.migrasap.beans.Auth;

/**
 *
 * @author daniel
 */
public class reqLsmwField {
    private Auth auth;
    private Data data;
    
    public reqLsmwField(){
        super();
    }

    public reqLsmwField(Auth auth, Data data) {
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private lsmwField lsmwField;
        
        public Data(){
            super();
        }

        public Data(lsmwField lsmwField) {
            this.lsmwField = lsmwField;
        }

        public lsmwField getLsmwField() {
            return lsmwField;
        }

        public void setLsmwField(lsmwField lsmwField) {
            this.lsmwField = lsmwField;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
