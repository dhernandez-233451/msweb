/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.mapping;

import java.util.List;
import mx.com.wwn.migrasap.beans.Error;
/**
 *
 * @author daniel
 */
public class resMapping {
    private Error error;
    private Data data;
    
    public resMapping(){
        super();
    }

    public resMapping(Error error, Data data) {
        this.error = error;
        this.data = data;
    }
    
    public class Data{
        private List<mapping> mapping;
        
        public Data(){
            super();
        }

        public Data(List<mapping> mapping) {
            this.mapping = mapping;
        }

        public List<mapping> getMapping() {
            return mapping;
        }

        public void setMapping(List<mapping> mapping) {
            this.mapping = mapping;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
