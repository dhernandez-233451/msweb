/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.localmanage;

import java.util.List;
import mx.com.wwn.migrasap.beans.Auth;

/**
 *
 * @author daniel
 */
public class reqMassiveImport {
    private Auth auth;
    private Data data;

    public reqMassiveImport() {
        super();
    }

    public reqMassiveImport(Auth auth, Data data) {
        this.auth = auth;
        this.data = data;
    }
    
    public class Data{
        private List<massiveImport> massiveImport;

        public Data() {
            super();
        }

        public Data(List<massiveImport> massiveImport) {
            this.massiveImport = massiveImport;
        }

        public List<massiveImport> getMassiveImport() {
            return massiveImport;
        }

        public void setMassiveImport(List<massiveImport> massiveImport) {
            this.massiveImport = massiveImport;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
