/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.sapconn;

import java.util.List;
import mx.com.wwn.migrasap.beans.Error;
/**
 *
 * @author daniel
 */
public class resSapConn {
    private Error error;
    private Data data;
    
    public resSapConn(){
        super();
    }

    public resSapConn(Error error, Data data) {
        this.error = error;
        this.data = data;
    }
    
    public class Data{
        private List<sapConn> sapConns;
        
        public Data(){
            super();
        }

        public Data(List<sapConn> sapConns) {
            this.sapConns = sapConns;
        }

        public List<sapConn> getSapConns() {
            return sapConns;
        }

        public void setSapConns(List<sapConn> sapConns) {
            this.sapConns = sapConns;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
