/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author daniel
 */
public class files {
    String directory = "/tmp/";
    String filename;
    String ext;
    byte[] filebytes;
    StreamedContent file;
    
    public StreamedContent convertToFile(String name, String b64) throws IOException{
        if(!name.equals("") && !b64.equals("")){
            ext = name.split("\\.")[name.split("\\.").length - 1];
            filename = name;
            filebytes = Base64.getDecoder().decode(b64);
            writeFile();
            readFile();
            removeFile();
        }
        return file;
    } 
    
    public String convertToBase64(UploadedFile ufile) throws IOException{
        String str = "";
        if(ufile != null){
            InputStream input = ufile.getInputstream();
            byte[] fileBytes = new byte[(int) ufile.getSize()];
            input.read(fileBytes, 0, fileBytes.length);
            input.close();
            str = Base64.getEncoder().encodeToString(fileBytes);
        }
        return str;
    }
    
    void writeFile() throws FileNotFoundException, IOException{
        OutputStream os = new FileOutputStream(directory+filename);
        os.write(filebytes);
        os.close();
    }
    
    void readFile() throws FileNotFoundException{
        InputStream is = new FileInputStream(directory+filename);
        file = new DefaultStreamedContent(is, "application/"+ext,filename);
    }
    
    void removeFile(){
        File f = new File(directory+filename);
        f.delete();
    }
}
//-----------Guías
//Recibe base64 y permite descarga del archivo
/*if(res.getError().getCode().equals("")){
            filebytes = Base64.getDecoder().decode(res.getData().getLocalManage().getContent());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Excelente", dbname));
            try (OutputStream out = new FileOutputStream("/home/daniel/Documentos/"+res.getData().getLocalManage().getDbname())) {
                out.write(filebytes);
                out.close();
            }
            
            InputStream stream = new FileInputStream("/home/daniel/Documentos/"+res.getData().getLocalManage().getDbname());
            file = new DefaultStreamedContent(stream, "application/sql",res.getData().getLocalManage().getDbname());
            File fr = new File("/home/daniel/Documentos/"+res.getData().getLocalManage().getDbname());
            fr.delete();
            return file;
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: "+res.getError().getCode(), res.getError().getMessage()));
            return null;
        }*/

//
/*
        //Carga de archivo y guardado ::: ya funciona
        System.out.println("D: "+uploadedFile.getFileName());
        //Path folder = Paths.get("/tmp/prueba");
        Path folder = Paths.get("/home/daniel/Documentos");
        //Path file = Files.createTempFile(folder, uploadedFile.getFileName(), ".docx");
        String partes[] = (uploadedFile.getFileName()).split("\\.");
        System.out.print("D: "+partes.length);
        String ext = "."+partes[1];
        System.out.print("D: "+ext);
        Path file = Files.createTempFile(folder, uploadedFile.getFileName(), ext);
        System.out.println("D: "+file.getFileName());
        try (InputStream input = uploadedFile.getInputstream()) {
            Files.copy(input, file, StandardCopyOption.REPLACE_EXISTING);   
        }
        catch(Exception e){
            
        }*/
        
        /*
        
        //Obtener base64 de un archivo ::: Ya funciona
        String imageStr = "";
        System.out.println("D: "+uploadedFile.getFileName());
         try (InputStream input = uploadedFile.getInputstream()) {   
            byte[] imageBytes = new byte[(int) uploadedFile.getSize()];
            input.read(imageBytes, 0, imageBytes.length);
            input.close();
            imageStr = Base64.getEncoder().encodeToString(imageBytes);
            System.out.println("D: "+imageStr);
        }
        catch(Exception e){
            System.out.println("D: "+e);
        }
        //Generar archivo de cadena base64 ::: Ya funciona
        byte[] data = Base64.getDecoder().decode(imageStr);
        try (OutputStream stream = new FileOutputStream("/home/daniel/Documentos/"+uploadedFile.getFileName())) {
            stream.write(data);
            stream.close();
        }
        catch(Exception e){
            System.out.println("D: "+e);
        }
        //
        */