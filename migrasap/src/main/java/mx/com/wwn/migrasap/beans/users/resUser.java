/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.wwn.migrasap.beans.users;
import java.util.List;
import mx.com.wwn.migrasap.beans.Error;
import mx.com.wwn.migrasap.beans.profiles.profile;
/**
 *
 * @author daniel
 */
public class resUser {
    private Error error;
    private Data data;

    public resUser() {
        super();
    }

    public resUser(Error error, Data data) {
        this.error = error;
        this.data = data;
    }
    
    public class Data{
        private List<profile> profiles;
        private List<user> users;

        public Data() {
            super();
        }

        public Data(List<profile> profiles, List<user> users) {
            this.profiles = profiles;
            this.users = users;
        }

        public List<profile> getProfiles() {
            return profiles;
        }

        public void setProfiles(List<profile> profiles) {
            this.profiles = profiles;
        }

        public List<user> getUsers() {
            return users;
        }

        public void setUsers(List<user> users) {
            this.users = users;
        }
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
